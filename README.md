# React Query useInfiniteQuery

## Resources

1. [What’s new in React Query 3](https://blog.logrocket.com/whats-new-in-react-query-3/)
2. [Build an Instagram-like infinite scrolling feed with React Query](https://blog.logrocket.com/build-instagram-infinite-scrolling-feed-react-query/)
