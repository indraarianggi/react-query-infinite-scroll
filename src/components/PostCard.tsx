import React from "react";
import { IPost } from "../interfaces";

type TPostCardProps = {
  post: IPost;
};

const PostCard = ({ post }: TPostCardProps) => {
  return (
    <div className="post-card">
      <h4>{post.author}</h4>
      <img src={post.download_url} alt={post.author} />
    </div>
  );
};

export default PostCard;
